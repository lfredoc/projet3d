import * as THREE from 'three'
import Stats from 'three/examples/jsm/libs/stats.module.js';

import { Octree } from 'three/examples/jsm/math/Octree.js';
import { OctreeHelper } from 'three/examples/jsm/helpers/OctreeHelper.js';

import { Capsule } from 'three/examples/jsm/math/Capsule.js';
import { GUI } from 'three/examples/jsm/libs/lil-gui.module.min.js';

// NOTE: three/addons alias or importmap does not seem to be supported by Parcel, use three/examples/jsm/ instead 
import {
  GLTFLoader
} from 'three/examples/jsm/loaders/GLTFLoader.js';

// Example of hard link to official repo for data, if needed
// const MODEL_PATH = 'https://raw.githubusercontent.com/mrdoob/three.js/r148/examples/models/gltf/LeePerrySmith/LeePerrySmith.glb';


// INSERT CODE HERE

let mixer = 0;
let mixer2 = 0

const scene = new THREE.Scene();
const aspect = window.innerWidth / window.innerHeight;
const clock = new THREE.Clock();

const camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.lookAt(scene.position);
camera.rotation.order = 'YXZ';

const directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
directionalLight.position.set(- 5, 25, - 1);
directionalLight.castShadow = true;
directionalLight.shadow.camera.near = 0.01;
directionalLight.shadow.camera.far = 500;
directionalLight.shadow.camera.right = 30;
directionalLight.shadow.camera.left = - 30;
directionalLight.shadow.camera.top = 30;
directionalLight.shadow.camera.bottom = - 30;
directionalLight.shadow.mapSize.width = 1024;
directionalLight.shadow.mapSize.height = 1024;
directionalLight.shadow.radius = 4;
directionalLight.shadow.bias = - 0.00006;
scene.add(directionalLight);

const fillLight1 = new THREE.HemisphereLight(0x4488bb, 0x002244, 0.5);
fillLight1.position.set(2, 1, 1);
scene.add(fillLight1);
//const light = new THREE.HemisphereLight(0xf9e59a, 0xacc9d3, 1);
const light = new THREE.AmbientLight(0xffffff, 1); // soft white light
scene.add(light);



// const controls = new OrbitControls(camera, renderer.domElement);
// controls.listenToKeyEvents(window); // optional

scene.environment = new THREE.TextureLoader().load("assets/models/Texture-base_baseColor.jpeg")
scene.background = new THREE.Color(0xacc9d3);

const container = document.getElementById('container');

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.VSMShadowMap;
renderer.outputEncoding = THREE.sRGBEncoding;
renderer.toneMapping = THREE.ACESFilmicToneMapping;
container.appendChild(renderer.domElement);

const stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.top = '0px';
container.appendChild(stats.domElement);



const GRAVITY = 30;
const STEPS_PER_FRAME = 5;


const worldOctree = new Octree();
const capsuleHeight = 0.65;
const playerCollider = new Capsule(new THREE.Vector3(-4.222, 3, 27.397), new THREE.Vector3(-4.222, 3 + capsuleHeight, 27.397), 0.35);


//cat = root.getObjectByName('cat');

const geometry = new THREE.CapsuleGeometry(0.5, capsuleHeight, 4, 8);
const material = new THREE.MeshBasicMaterial();
material.transparent = true;
material.opacity = 0.0;

let visualPlayer = new THREE.Mesh(geometry, material);

scene.add(visualPlayer);

visualPlayer.position.set(-4.222, 3, 27.397);


const playerVelocity = new THREE.Vector3();
const playerDirection = new THREE.Vector3();

let playerOnFloor = false;
let mouseTime = 0;

const keyStates = {};

const vector1 = new THREE.Vector3();
const vector2 = new THREE.Vector3();
const vector3 = new THREE.Vector3();


document.addEventListener('keydown', (event) => {

  keyStates[event.code] = true;

});

document.addEventListener('keyup', (event) => {

  keyStates[event.code] = false;

});

// container.addEventListener('mousedown', () => {

//   document.body.requestPointerLock();

//   mouseTime = performance.now();

// });



document.body.addEventListener('mousemove', (event) => {

  if (document.pointerLockElement === document.body) {

    camera.rotation.y -= event.movementX / 500;
    camera.rotation.x -= event.movementY / 500;

  }

});

window.addEventListener('resize', onWindowResize);

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);

}

function playerCollisions() {

  const result = worldOctree.capsuleIntersect(playerCollider);

  playerOnFloor = false;

  if (result) {

    playerOnFloor = result.normal.y > 0;

    if (!playerOnFloor) {

      playerVelocity.addScaledVector(result.normal, - result.normal.dot(playerVelocity));

    }

    playerCollider.translate(result.normal.multiplyScalar(result.depth));

  }

}

function updatePlayer(deltaTime) {

  let damping = Math.exp(- 4 * deltaTime) - 1;

  if (!playerOnFloor) {

    playerVelocity.y -= GRAVITY * deltaTime;

    // small air resistance
    damping *= 0.2;

  }

  playerVelocity.addScaledVector(playerVelocity, damping);

  const deltaPosition = playerVelocity.clone().multiplyScalar(deltaTime);
  playerCollider.translate(deltaPosition);

  playerCollisions();
  visualPlayer.position.copy(playerCollider.end);
  //camera.lookAt(visualPlayer);


  var relativeCameraOffset = new THREE.Vector3(0, 1, 4);

  var cameraOffset = relativeCameraOffset.applyMatrix4(visualPlayer.matrixWorld);

  camera.position.x = cameraOffset.x;
  camera.position.y = cameraOffset.y;
  camera.position.z = cameraOffset.z;
  //camera.lookAt(visualPlayer);
  // camera.position.z = visualPlayer.position.z;
  // camera.position.x = visualPlayer.position.x;
  // camera.position.y = visualPlayer.position.y + 3;
}


function getForwardVector() {

  camera.getWorldDirection(playerDirection);
  playerDirection.y = 0;
  playerDirection.normalize();

  return playerDirection;

}

function getSideVector() {

  camera.getWorldDirection(playerDirection);
  playerDirection.y = 0;
  playerDirection.normalize();
  playerDirection.cross(camera.up);

  return playerDirection;

}


function controls(deltaTime) {

  let rotateAngle = Math.PI / 2 * deltaTime;
  // gives a bit of air control
  const speedDelta = deltaTime * (playerOnFloor ? 25 : 8);
  if (keyStates['KeyA']) {

    visualPlayer.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
    //playerVelocity.add(getSideVector().multiplyScalar(- speedDelta));

  }
  if (keyStates['KeyW']) {

    playerVelocity.add(getForwardVector().multiplyScalar(speedDelta));

  }

  if (keyStates['KeyS']) {

    playerVelocity.add(getForwardVector().multiplyScalar(- speedDelta));

  }

  if (keyStates['KeyD']) {

    //playerVelocity.add(getSideVector().multiplyScalar(speedDelta));
    visualPlayer.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);

  }

  if (playerOnFloor) {

    if (keyStates['Space']) {

      playerVelocity.y = 10;

    }

  }
  camera.lookAt(visualPlayer.position);
}


function dumpObject(obj, lines = [], isLast = true, prefix = '') {
  const localPrefix = isLast ? '└─' : '├─';
  lines.push(`${prefix}${prefix ? localPrefix : ''}${obj.name || '*no-name*'} [${obj.type}]`);
  const newPrefix = prefix + (isLast ? '  ' : '│ ');
  const lastNdx = obj.children.length - 1;
  obj.children.forEach((child, ndx) => {
    const isLast = ndx === lastNdx;
    dumpObject(child, lines, isLast, newPrefix);
  });
  return lines;
}

function loadData() {
  const worldLoader = new GLTFLoader();
  worldLoader.setPath('assets/models/').load('medieval_map.glb', gltfReader);
}


function gltfReader(gltf) {
  let testModel = null;


  testModel = gltf.scene;

  if (testModel != null) {
    console.log("Model loaded:  " + testModel);
    scene.add(gltf.scene);

    mixer = new THREE.AnimationMixer(testModel);
    if (gltf.animations.length) {
      mixer.clipAction(gltf.animations[0]).play();
      animate();
    }
    console.log(dumpObject(testModel).join('\n'));


    worldOctree.fromGraphNode(gltf.scene);

    gltf.scene.traverse(child => {

      if (child.isMesh) {

        child.castShadow = true;
        child.receiveShadow = true;

        if (child.material.map) {

          child.material.map.anisotropy = 4;

        }

      }

    });

    const helper = new OctreeHelper(worldOctree);
    helper.visible = false;
    scene.add(helper);

    const gui = new GUI({ width: 200 });
    gui.add({ debug: false }, 'debug')
      .onChange(function (value) {

        helper.visible = value;

      });



  } else {
    console.log("Load FAILED.  ");
  }

}

loadData();

{
  const catLoader = new GLTFLoader();
  catLoader.setPath('assets/models/').load('cara.glb', (gltf) => {
    const root = gltf.scene;
    root.position.y = root.position.y - capsuleHeight - 0.1;

    if (root != null) {
      console.log("Model loaded:  " + root);
      visualPlayer.add(root)

      mixer2 = new THREE.AnimationMixer(root);
      if (gltf.animations.length) {
        mixer2.clipAction(gltf.animations[0]).play();
        animate();
      }
    }
    console.log(root.position);

    //scene.add(root);

  });
}


// Main loop
function animate(time) {
  time *= 0.001;  // convert to seconds
  const deltaTime = Math.min(0.05, clock.getDelta()) / STEPS_PER_FRAME;

  // we look for collisions in substeps to mitigate the risk of
  // an object traversing another too quickly for detection.

  for (let i = 0; i < STEPS_PER_FRAME; i++) {

    controls(deltaTime);

    updatePlayer(deltaTime);

  }
  const delta = clock.getDelta();

  mixer.update(deltaTime);
  mixer2.update(deltaTime * 4);
  renderer.render(scene, camera);

  stats.update();
  //camera.lookAt(playerCollider.position);
  requestAnimationFrame(animate);

}
animate();